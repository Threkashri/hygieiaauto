const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
const schema = new mongoose.Schema({
  name: {
    type: String,
    default: '',
    unique: true
  },
  image: {
    type: String,
    default: ''
  },
  lastRefreshedTime: {
    type: String,
    default: ''
  },
  selected: {
    type: Boolean,
    default: false
  }
});
let collectors = mongoose.model('collectors', schema);
module.exports = {
  collectors: collectors
};
