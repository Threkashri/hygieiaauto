const {rootPath} = require('./rootPathEntity');
var shell = require('shelljs');
var fs = require('fs');
const Filehound = require('filehound');
const path = require('path');
const { spawn } = require('child_process');

var component = {
  writeContent: function(a, b) {
    fs.writeFile(a, b, {
      encoding: "utf-8"
    }, function() {
      console.log("done!")
    });
  },
  getRootPath: function(req, res) {
    rootPath.find({}, function(err, result) {
      if (err)
        throw err;
      else
        res.send(result);
      }
    )
  },
  setRootPath: function(req, res) {
    console.log('roo', req.body.rootPath);
    try {
      stats1 = fs.lstatSync(req.body.rootPath + '/UI');
      stats2 = fs.lstatSync(req.body.rootPath + '/api');
      stats3 = fs.lstatSync(req.body.rootPath + '/collectors');
      if (stats1.isDirectory() && stats2.isDirectory() && stats3.isDirectory()) {
        rootPath.find({}, function(err, result) {
          if (err)
            throw err;
          else {
            if (result.length != 0) {
              result[0].rootPath = req.body.rootPath;
              result[0].save(function(err) {
                if (err)
                  throw err;
                else {
                  res.send('root path set')
                }
              });
            } else {
              let rp = new rootPath({rootPath: req.body.rootPath});
              rp.save(function(err) {
                if (err)
                  throw err;
                else {
                  res.send('root path set')
                }
              });
            }
          }
        })
        let apiFileName1 = './server/restartJavaApp/bash/mvnInstall.sh';
        let stApi1 = '#! /usr/bin/bash';
        let strApi1 = req.body.rootPath;
        let line1Api1 = 'cd "'+strApi1+'"';
        let str1Api1 = 'mvn clean install package';
        aaApi = stApi1+'\n'+line1Api1+'\n'+str1Api1;
        fs.writeFileSync(
              apiFileName1,
              aaApi,
              {
                  encoding: "utf-8"
              },
              function(){ console.log("done!") }
          );

    } else {
      res.send('invalid path');
    }
    }catch(e) {
      console.log("not found",e);
      res.send('invalid path')
    }
    let strCheck = req.body.rootPath+'/api/target';
    Filehound.create()
      .ext('jar')
      .paths(strCheck)
      .find((err, jarFiles) => {
        if (err) return console.error("handle err", err);

        console.log(jarFiles);
        if(jarFiles.length==0) {
          let pathToBash1 = path.join(__dirname, `./bash/mvnInstall.sh`);
          const deploySh = spawn('sh', [ pathToBash1 ], {
          detach: true
        });
        }
        //   res.send('No Jar');
        // }else{
        let b = true;
        console.log('roo',req.body.rootPath);
        try{
        stats1 = fs.lstatSync(req.body.rootPath+'/UI');
        stats2 = fs.lstatSync(req.body.rootPath+'/api');
        stats3 = fs.lstatSync(req.body.rootPath+'/collectors');
        if(stats1.isDirectory() && stats2.isDirectory() && stats3.isDirectory()){
          console.log("inside dir");
          // write bashfiles here
          let nameArray = ['bamboo', 'bitbucket', 'github', 'gitlab', 'jenkins', 'jira', 'sonar'];
          let dummy = req.body.rootPath;
          let arrOfContent = [];
          for (var i = 0; i < nameArray.length; i++) {
            let fileName = './server/restartJavaApp/bash/'+nameArray[i]+'.sh';
            let st = '#! /usr/bin/bash';
            let nameOfJar;
            let toolType;
            let aa;
            let line1;
            let str1;
            if(nameArray[i] == 'jenkins' || nameArray[i]=='bamboo' || nameArray[i]=='sonar') {
              toolType = 'build';
            } else
            if(nameArray[i] == 'jira') {
              toolType = 'feature';
            } else
            if(nameArray[i] == 'bitbucket' || nameArray[i]=='github' || nameArray[i]=='gitlab') {
              toolType = 'scm';
            }
            let str = dummy+'/collectors/'+toolType+'/'+nameArray[i]+'/target';
            console.log(str);
            let toolName = nameArray[i];
            Filehound.create()
              .ext('jar')
              .paths(str)
              .find((err, jarFiles) => {
                if (err) return console.error("handle err", err);

                // console.log(jarFiles);
                let nameOfJar = jarFiles[0].split('\\');
                let nameOfJarFile = nameOfJar[nameOfJar.length-1];
                line1 = 'cd "'+str+'"';
                str1 = 'java -jar '+nameOfJarFile+' --spring.config.name='+toolName+' --spring.config.location='+dummy+'/collectors/'+toolType+'/'+toolName+'/application.properties';
                aa = st+'\n'+line1+'\n'+str1;
                arrOfContent.push(aa);
                console.log('$$$$\n', aa);
                fs.writeFileSync(
                      fileName,
                      aa,
                      {
                          encoding: "utf-8"
                      },
                      function(){ console.log("done!") }
                  );
            });
          }
          let strApi = dummy+'/api/target';
          Filehound.create()
            .ext('jar')
            .paths(strApi)
            .find((err, jarFiles) => {
              if (err) return console.error("handle err", err);

              // console.log(jarFiles);
              let nameOfJar = jarFiles[0].split('\\');
              let nameOfJarFile = nameOfJar[nameOfJar.length-1];

              let stApi = '#! /usr/bin/bash';
              let apiFileName = './server/restartJavaApp/bash/api.sh';
              let line1Api = 'cd "'+strApi+'"';
              str1Api = 'java -jar '+nameOfJarFile+' --spring.config.name=api --spring.config.location='+dummy+'/api'+'/dashboard.properties';
              aaApi = stApi+'\n'+line1Api+'\n'+str1Api;
              // arrOfContent.push(aaApi);
              console.log('$$$$\n', aaApi);
              fs.writeFileSync(
                    apiFileName,
                    aaApi,
                    {
                        encoding: "utf-8"
                    },
                    function(){ console.log("done!") }
                );
          });
        }
        }
          catch(e){
            console.log("not found",e);
            res.send('invalid path')
          }
        // }
        });

  }
}
module.exports = component;
