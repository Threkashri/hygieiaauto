const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
const schema = new mongoose.Schema({
  rootPath: {
    type: String,
    default: ''
  }
});
let rootPath = mongoose.model('rootPath', schema);
module.exports = {
  rootPath: rootPath
};
