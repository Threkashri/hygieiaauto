'use strict';
const router = require('express').Router();
var bcrypt   = require('bcrypt-nodejs');
let driver = require('../config/neo4j');
let session = driver.session();
const {user} = require('./userEntity');
router.post('/checkUser', function(req, res) {
  // let query = 'match(n:user{username:"'+req.body.username+'"}) return n';
  // session.run(query).then(function(result){
  //   if(result.records.length != 0) {
  //     let dbPwd = result.records[0]._fields[0].properties.password;
  //     if(bcrypt.compareSync(req.body.password, dbPwd)) {
  //     res.send('success');
  //   } else {
  //     res.send('failure');
  //   }
  //   } else {
  //     res.send('failure');
  //   }
  // })
  user.find({username: req.body.username}, function(err, result) {
    if (err)
      throw err;
    else {
      if(result.length == 1) {
          let dbPwd = result[0].password;
          if(bcrypt.compareSync(req.body.password, dbPwd)) {
          res.send('success');
        }
      } else {
        res.send('failure');
      }
      console.log(result);
    }
  })
});
router.post('/signup', function(req, res) {
  // let query1 = 'match(n:user{username:"'+req.body.username+'"}) return n';
  // session.run(query1).then(function(result) {
  //   if(result.records.length == 0) {
  //     let pwd = bcrypt.hashSync(req.body.password, bcrypt.genSaltSync(8), null);
  //     let query = 'merge(n:user{username:"'+req.body.username+'",password:"'+pwd+'"}) return n';
  //     session.run(query).then(function(result){
  //       if(result.records.length != 0) {
  //         res.send('success');
  //       } else {
  //         res.send('failure');
  //       }
  //     })
  //   } else {
  //     res.send('user already present');
  //   }
  // });
  user.find({username: req.body.username}, function(err, result) {
    if (err)
      throw err;
    else {
      if(result.length == 0) {
        let pwd = bcrypt.hashSync(req.body.password, bcrypt.genSaltSync(8), null);
        let userA = new user({username: req.body.username, password: pwd});
        userA.save(function(err) {
          if (err)
            throw err;
          else {
            res.send('user added successfully');
          }
        });
      } else {
        res.send('user already present');
      }
      console.log(result);
    }
  });
});
module.exports = router;
