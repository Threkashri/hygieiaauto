const React = require('react');
const {Link} = require('react-router');
const {hashHistory} = require('react-router');
import { Button, Checkbox, Form, Card,Input,Icon } from 'semantic-ui-react';
// import Cookie from 'react-cookie';
import Cookies from 'universal-cookie';
const cookies = new Cookies();
const ReactToastr = require('react-toastr');
const {ToastContainer} = ReactToastr;
const ToastMessageFactory = React.createFactory(ReactToastr.ToastMessage.animation);

class Login extends React.Component {
  constructor() {
    super();
    this.state = {
      username:'',
      password:'',
      alertMsg:false,
      collectorsArray: []
    };
    this.checkCredentials = this.checkCredentials.bind(this);
    this.LoginUser = this.LoginUser.bind(this);
    this.checkforPasswordMismatch = this.checkforPasswordMismatch.bind(this);
    this.handleUserName = this.handleUserName.bind(this);
    this.handlePassword = this.handlePassword.bind(this);
    this.checkForCollectors = this.checkForCollectors.bind(this);
  }
  checkforPasswordMismatch() {
    console.log("inside check for password mismatch alert");
    let context = this;
    this.refs.asd.warning(
      'Password mismatch',
      '', {
      timeOut: 3000,
      extendedTimeOut: 3000
    }
  );
  }
  checkForCollectors() {
    $.ajax({
       url:"/restart/getCollectors",
       type: 'GET',
       success: function(res)
       {
         console.log(res);
        if(res.length!=0){
          this.setState({collectorsArray:res.length});
        }else {
          this.setState({collectorsArray:0});
        }

       }.bind(this),
       error: function(err)
       {
         console.log('inside failure');
         //console.log(err.responseText);
       }.bind(this)
     });
  }
  handleUserName(e) {
    this.setState({username: e.target.value});
    //console.log("username ",this.state.username);
  }
  handlePassword(e) {
    this.setState({password: e.target.value});
  }

  checkCredentials() {
    if(this.state.username == "" || this.state.password == "") {
      return "nodata";
    } else if(this.state.checkDataFromDb) {
      return "invalid";
    }else{
      return "success";
    }
  }
  LoginUser() {
    if(this.checkCredentials() == "nodata"){}else{
      let context = this;
      $.ajax({
        url:"/users/checkUser",
        type: 'POST',
        datatype: 'JSON',
        data:{username :this.state.username,password:this.state.password},
        success: function(res)
        {
          this.checkForCollectors();
          console.log('inside success',res);
          if(res == 'failure') {
            console.log('failure');
            context.checkforPasswordMismatch();
          }else if(res == 'success'){
            cookies.set('username', this.state.username);
            if(this.state.collectorsArray != 0) {
              hashHistory.push('/selection');
            }else{
              hashHistory.push('/configure');
            }
        }
        }.bind(this),
        error: function(err)
        {

        }.bind(this)
      });
    }
  }
  render() {
    let loginPage;
    let alertMessages = "";

      loginPage = (<div className='container' style={{marginTop:'7%'}}>

  <form method='POST' style={{margin:'auto',width:'45%'}}><ul className="nav nav-tabs">
<li className="active"><a href="#" style={{color:'white',background: 'transparent'}}>Admin</a></li>
</ul>
<h2 style={{color:'white', textAlign:'center'}}>Admin Login</h2>
      <div className="form-group">
        <input type="text" style={{textAlign:'center'}} onChange={this.handleUserName} required className="form-control" id="email" placeholder="Enter your username" name="email"/>
      </div>
      <div className="form-group">
        <input type="password" style={{textAlign:'center'}} onChange={this.handlePassword} required className="form-control" id="pwd" placeholder="Enter your password" name="pwd"/>
      </div><center><button type="button" onClick={this.LoginUser} className="btn btn-default" style={{margin:'auto',background: '#80868e',color: 'white',fontStyle: 'normal',fontWeight: '400',fontSize: '14px',fontFamily: 'inherit'}}><i className="fa fa-thumbs-o-up" aria-hidden="true"/>&nbsp;&nbsp;Log in</button></center>
    </form>
  </div>);
let loginPageContent = '';
if(!cookies.get('username')) {
  loginPageContent = (  <div>
      {loginPage}
      <ToastContainer ref='asd'
        toastMessageFactory={ToastMessageFactory}
        className='toast-top-center'/>
    </div>);
} else {
  hashHistory.push('/configure');
}
return(<div>
  <nav className="navbar navbar-inverse navbar-fixed-top">
      <div className="container-fluid">
        <div className="navbar-header">
          <img src="./../../image/CDAPLogo.png" class="img-responsive" alt="CDAP" style={{float:'left',width:'13%'}}/>
          <a className="navbar-brand" href="#/selection" style={{color:'white',margin:'1.5% 0 0 1%',fontSize:'20px'}}>Continuous Delivery Analytics Platform</a>
        </div>
      </div>
    </nav>
{loginPageContent}
</div>
);
}
}
module.exports = Login;
