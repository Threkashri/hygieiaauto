const React = require('react');
const {hashHistory} = require('react-router');
import Card from './Card.jsx';
import Cookies from 'universal-cookie';
const cookies = new Cookies();
class LandingPage extends React.Component {
  constructor() {
    super();
    this.state = {
      collectorsArray: [
      {toolName: 'Jenkins', refresh: true, image: 'https://jenkins.io/images/226px-Jenkins_logo.svg.png'},
      {toolName: 'Bamboo',  refresh: true, image: 'http://blog.falafel.com/wp-content/uploads/2015/05/logoBambooPNG.png'},
      {toolName: 'Sonar',  refresh: true, image: 'https://docs.sonarqube.org/download/attachments/6389860/SONARNEXT?version=1&modificationDate=1440424676000&api=v2'},
      {toolName: 'Jira',  refresh: true, image: 'https://a.slack-edge.com/ae7f/plugins/jira/assets/service_512.png'},
      {toolName: 'BitBucket',  refresh: true, image: 'https://cdn2.iconfinder.com/data/icons/designer-skills/128/bitbucket-repository-svn-manage-files-contribute-branch-512.png'},
      {toolName: 'GitHub',  refresh: true, image: 'https://image.flaticon.com/icons/png/512/25/25231.png'},
      {toolName: 'GitLab',  refresh: true, image: 'https://img.stackshare.io/service/880/lmalkclL.png'}
    ],
      isOpen: true
    };
    this.refreshMethod = this.refreshMethod.bind(this);
  }

  componentWillMount(){
    this.refreshMethod();
    $.ajax({
       url:"/restart/getCollectors",
       type: 'GET',
       success: function(res)
       {
         console.log('-------',res);
         let arr = [{toolName: 'API', refresh: true, image: 'https://crowdsourcedtesting.com/resources/wp-content/uploads/2016/01/010915_1247_Beforegoing3.png'}];
         for (var i = 0; i < res.length; i++) {
           for (var j = 0; j < this.state.collectorsArray.length; j++) {
             console.log(this.state.collectorsArray[j].toolName.toLowerCase()+'***'+res[i].name);
           if(res[i].selected && this.state.collectorsArray[j].toolName.toLowerCase()==res[i].name) {
             arr.push(this.state.collectorsArray[j]);
           }
           if(i == res.length-1) {
             this.setState({collectorsArray:arr});
           }
         }
       }
        //  this.setState({collectorsArray:collectorsArray});
       }.bind(this),
       error: function(err)
       {
         console.log('inside failure');
         //console.log(err.responseText);
       }.bind(this)
     });
  }

  refreshMethod(){
    let collectorsArray = this.state.collectorsArray;
    $.ajax({
       url:"/restart/getCollectors",
       type: 'GET',
       success: function(res)
       {
         res.map((item,i)=>{
           let index = collectorsArray.findIndex(x => x.toolName.toLowerCase() == item.name);
           let diff = new Date().getTime() - item.lastRefreshedTime;
           if(diff < 300000){
             collectorsArray[index].refresh = false;
             setTimeout(()=>{
               console.log('setTimeout');
               collectorsArray[index].refresh = true;
             this.setState({collectorsArray:collectorsArray});
           }, 300000-diff);
           console.log('getLastRefreshedTime',index);
           }
         })
         this.setState({collectorsArray:collectorsArray});
       }.bind(this),
       error: function(err)
       {
         console.log('inside failure');
         //console.log(err.responseText);
       }.bind(this)
     });
  }
  logout() {
    cookies.remove('username');
    // hashHistory.push('/');
    location.reload();
  }
  render() {
    let landingPageContent = '';
    if(cookies.get('username')) {
      landingPageContent = (<div>
        <nav className="navbar navbar-inverse navbar-fixed-top">
            <div className="container-fluid">
              <div className="navbar-header">
                <img src="./../../image/CDAPLogo.png" class="img-responsive" alt="CDAP" style={{float:'left',width:'13%'}}/>
                <a className="navbar-brand" href="#/selection" style={{color:'white',margin:'1.5% 0 0 1%',fontSize:'20px'}}>Continuous Delivery Analytics Platform</a>
              </div>
              <div className="navbar-header navbar-right" style={{zIndex:1000,margin:'1%', cursor:'pointer'}} onClick={this.logout.bind(this)}>
                <span className="fa-stack fa-lg"><i className="fa fa-circle fa-stack-2x"></i> <i className="fa fa-power-off fa-stack-1x" style={{color:'white'}}></i></span>
              </div>
            </div>
          </nav>
        <div className='container'>
          <div className='row' style={{marginTop:"6%"}}>
            {
              this.state.collectorsArray.map((item, key) => {
                return (<div key={key} className="col-md-3 col-sm-6 col-lg-3">
                      <Card cardTitle={item.toolName} refresh={item.refresh} refreshMethod={this.refreshMethod} cardImage={item.image}  cardContent={item.content} rootPath='C:\Hygieia-master\Hygieia-master'/>
                </div>);
              })

            }
          </div>

        </div>
      </div>);
    } else {
      hashHistory.push('/');
    }
        return(
          <div>
            {landingPageContent}
          </div>
        );
}
}

module.exports = LandingPage;
